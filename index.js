const read = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

(async () => {
  let total = await getInput();

  for (let i = 0; i < total; i++) {
    let size = await getInput();
    let values = await getInput();
    let target = await getInput();

    values = values.split(' ').sort((a, b) => a - b);

    let result = search(target, values);

    console.log(result);
  }

  read.close();
})();

async function getInput() {
  return new Promise(resolve => {
    read.question('', input => {
      resolve(input);
    }); 
  });
}

function search(needle, haystack, index = 0) {
  let mid = Math.floor(haystack.length / 2);

  if (needle === haystack[mid]) {
    return mid;
  } else if (needle < haystack[mid] && haystack.length > 1) {
    let slice = haystack.slice(0, mid - 1);

    return search(needle, slice, index);
  } else if (needle > haystack[mid] && haystack.length > 1) {
    let slice = haystack.slice(mid + 1);

    return search(needle, slice, index + slice.length);
  } else {
    return -1;
  }
}
